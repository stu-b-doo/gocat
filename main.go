// Package gocat demos using doublestar glob to mimick unix cat
package main

import (
	"bufio"
	"fmt"
	glob "github.com/bmatcuk/doublestar"
	"flag"
	"os"
	fp "path/filepath"
)

const help = `Usage: gocat <glob>
Example: gocat "**/*.txt" will print all text files in subdirectories of the current directory
The glob pattern must be enclosed in quotes`

const maxFiles = 1000

func main() {

flag.Parse()

	// pattern is first arg. Must be in quotes
	if len(os.Args) < 2{
		Fatal(help)
	}

	// glob hits (FromSlash converts / to \ for Windows compatibility)
	pattern := fp.FromSlash(os.Args[1])
	files, err := glob.Glob(pattern)
	if err != nil {
		Fatal(err)
	}

	// max hit check
	if len(files) > maxFiles {
		Fatal("Pattern matches too many files to print")
	}

	// process each file that match glob
	for _, path := range files {
		printFile(path)
	}
}

// printFile prints contents of file at filepath to stdout.
func printFile(filePath string) {
	file, err := os.Open(filePath)
	defer file.Close()

	if err != nil {
		// skip file silently
		return
	}

	// scan file by lines and print
	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		fmt.Println(scanner.Text())
	}

}

func Fatal(msg ...interface{}) {
	fmt.Println(msg)
	os.Exit(1)
}
